<?php
include "header.php";
?>
<div id="" >
	<div id="header" class="header">
		<div class="container">
			<nav>
				<div class="d-flex bd-highlight mb-3">
					<div class="mr-auto p-2 bd-highlight">
						<div class="brand-logo">
							<a href="<?= base_url()?>">
								<img src="<?= base_url()?>assets/home_assets/img/brand-logo-white.png">
							</a>							
						</div>
					</div>
					
				</div>
			</nav>
		</div>		
	</div>
	<section id="banner fix-baner">
		<div class="banner ">
		<?php if ($detail_member['avatar']==null) { ?>
				<img src="<?= base_url('assets/gambar_distributor/gambar_toko/no-photo.jpg') ?>">
			<?php } else { ?>
				<img src="<?= base_url('assets/gambar_distributor/gambar_toko/') . $detail_member['gambar_toko'] ?>">
			<?php } ?>
			
			<div class="bg-gradient-soft"></div>
		</div>	
	</section>
	<div class="pb-5">
		<div class="container">
			<div class="img-logo">
			<?php if ($detail_member['avatar']==null) { ?>
				<img src="<?= base_url('assets/gambar_distributor/avatar/noimage.png') ?>">
			<?php } else { ?>
				<img src="<?= base_url('assets/gambar_distributor/avatar/') . $detail_member['avatar'] ?>">
			<?php } ?>
				
			</div>
			<div class="row my-5">
				<div class="col-12 col-sm-12 col-md-6 col-lg-6">
					<div class="mb-2">
						<h5 class="blue">
							<b><?= $detail_member['nama'] ?></b>
						</h5>
						<h6 class="blue">

						</h6>
						<h6 class="blue">
							Alamat : <?= $detail_member['alamat'] ?>
						</h6>
					</div>					
				</div>
				<div class="col-12 col-sm-12 col-md-6 col-lg-6">
					<div class="row">
						<div class="col-5 col-sm-5 col-md-5 col-lg-5">
						</div>
						<div class="col-7 col-sm-7 col-md-7 col-lg-7">
							<div class="box-view px-2" align="center">
								<i class="fas fa-child"></i> Dilihat <?= $banyak_view ?> Orang
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="bg-img-about">
			<img src="<?= base_url()?>assets/home_assets/img/bg-img.png">
		</div>
		<div class="container">
			<div>
				<h5 class="my-3"><b>Varian Produk</b></h5>

				<div class="owl-nav">
					<div class="owl-next">
						<i class="fas fa-chevron-circle-left"></i>
					</div>
				</div>
				<div class="owl-carousel owl-theme owl-loaded margin-owl ">
					<div class="owl-stage-outer pl-2">
						<div class="owl-stage">

						<?php foreach ($all_produk_item as $ap_item) : ?>
							<div class="owl-item">
								<div class="box-product bg-white ">
									<div class="img-cover-product">
										<a href="detail-produk.php">
											<img src="<?= base_url('assets/gambar_item/thumbnail/') . $ap_item['thumbnail'] ?>">
										</a>										
									</div>
									<div class="py-2 px-2">
										<h6><b><?= substr($ap_item['nama'], 0,15).''?></b></h6>
										<p>
										<?php echo substr($ap_item['deskripsi'], 5,50).'...'; ?>
										</p>
										<div class="row">
											<div class="col-3">
												<small class="grey"> Beli Di </small>
											</div>
											<div class="col-9">
												<ul class="link-buy">
													<li>
														<a href="<?= $detail_member['link_lazada'] ?>" target="blank" class="icon-link">
															<img src="<?= base_url()?>assets/home_assets/img/lazada.png">
														</a>
													</li>
													<li>
														<a href="<?= $detail_member['link_tokopedia'] ?>" target="blank" class="icon-link">
															<img src="<?= base_url()?>assets/home_assets/img/tokopedia.png">
														</a>
													</li>
													<li>
														<a href="<?= $detail_member['link_bukalapak'] ?>" target="blank" class="icon-link">
															<img src="<?= base_url()?>assets/home_assets/img/bl.png">
														</a>
													</li>
													<li>
														<a href="<?= $detail_member['link_instagram'] ?>" target="blank" class="icon-link">
															<img src="<?= base_url()?>assets/home_assets/img/ig.png">
														</a>
													</li>
													<li>
														<a href="<?= $detail_member['link_shopee'] ?>" target="blank" class="icon-link">
															<img src="<?= base_url()?>assets/home_assets/img/shopee.png">
														</a>
													</li>
													<li>
														<a href="<?= $detail_member['link_blibli'] ?>" target="blank" class="icon-link">
															<img src="<?= base_url()?>assets/home_assets/img/blibli.png">
														</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach;?>


						</div>
					</div>						
				</div>

				<div>
					<h5 class="mb-2 blue"><b>Kunjungi Toko Online <?= $detail_member['nama'] ?></b></h5>
					<h5>Instagram</h5>
					<p>
						<a href="<?= $detail_member['link_instagram'] ?>"><?= $detail_member['link_instagram'] ?></a>
					</p>
					<h5>Lazada</h5>
					<p>
						<a href="<?= $detail_member['link_lazada'] ?>"><?= $detail_member['link_lazada'] ?></a>
					</p>
					<h5>Tokopedia</h5>
					<p>
						<a href="<?= $detail_member['link_tokopedia'] ?>"><?= $detail_member['link_tokopedia'] ?></a>
					</p>
					<h5>Bukalapak</h5>
					<p>
						<a href="<?= $detail_member['link_bukalapak'] ?>"><?= $detail_member['link_bukalapak'] ?></a>
					</p>
					<h5>Shopee</h5>
					<p>
						<a href="<?= $detail_member['link_shopee'] ?>"><?= $detail_member['link_shopee'] ?></a>
					</p>
				</div>

			</div>
		</div>

	</div>
</div>



</div>

<?php
include "footer.php";
?>